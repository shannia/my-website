from . import models
from django import forms
import datetime

class mySchedule(forms.ModelForm):
    kegiatan = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "schedule full",
                "required" : True,
                "placeholder": "Nama Kegiatan",
                }))
    tempat = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "schedule full",
                "required" : True,
                "placeholder": "Tempat",
                }))
    kategori = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "schedule full",
                "required" : True,
                "placeholder":"Kategori",
                }))
    tanggal = forms.DateField(widget=forms.SelectDateWidget(attrs={
                "class" : "datefield jadwalfields",
                "required" : True,
                }), initial=datetime.datetime.now())
    waktu = forms.TimeField(widget=forms.TimeInput(attrs={
                "class" : "datefield jadwalfields",
                "placeholder":"00:00",
                "required" : True,
                }), initial=datetime.datetime.now()) 
    
    class Meta: 
            model = models.Schedule
            fields = ['kegiatan','kategori','tempat','tanggal', 'waktu']



