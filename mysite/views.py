from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import mySchedule
from .models import Schedule
from django.utils import timezone
import datetime

def home(request):
    return render(request,'HOME.html')

def aboutme(request):
    return render(request, 'ABOUTME.html')

def experience(request):
    return render(request, 'EXPERIENCES.html')

def project(request):
    return render(request, 'PROJECT.html')

def gallery(request):
    return render(request, 'GALLERY.html')

def contact(request):
    return render(request, 'CONTACT.html')

def create_schedule(request):
    form = mySchedule()
    if request.method =="POST":
        form = mySchedule(request.POST)
        if form.is_valid():
            form.save()
            return redirect('mysite:schedule')
    return render(request, "MakeSchedule.html",{'form':form})

def schedule(request):
    schedules = Schedule.objects.order_by("tanggal","waktu")
    response = {
        'schedule' : schedules,
        'server_time' : datetime.datetime.now()
    }
    return render(request, "SCHEDULE.html", response)

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Schedule.objects.get(id=id).delete()
    return redirect('mysite:schedule')
    