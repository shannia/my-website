from django.db import models
import datetime
from django.utils import timezone

class Schedule(models.Model):
    kegiatan = models.CharField(max_length=40)
    tempat = models.CharField(max_length=40)
    kategori= models.CharField(null=True, max_length=40)
    tanggal = models.DateField(default=timezone.now)
    waktu = models.TimeField(default=timezone.now)
    
    def __str__(self):
        return self.kegiatan